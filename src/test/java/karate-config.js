function fn() {
  var env = karate.env; // get java system property 'karate.env'
  karate.log('karate.env system property was:', env);
  if (!env) {
    env = 'dev'; // a custom 'intelligent' default
  }
  var config = { // base config JSON
    appId: 'my.app.id',
    appSecret: 'my.secret',
    urlBase: 'http://localhost:3333/api'
   };

  if (env == 'stage') {
    // over-ride only those that need to be
    config.urlBase = 'https://api.realworld.io-stage/api';
  } else if (env == 'e2e') {
    config.urlBase = 'https://api.realworld.io-e2e/api';
  }

  config.urlLogin = config.urlBase + '/users/login';
  config.urlArticles = config.urlBase + '/articles';

  config.login = {};
  config.login.email = "matheus.kerr@bestcode.com.br";
  config.login.password = "best123";

  // don't waste time waiting for a connection or if servers don't respond within 5 seconds
  karate.configure('connectTimeout', 5000);
  karate.configure('readTimeout', 5000);
  return config;
}