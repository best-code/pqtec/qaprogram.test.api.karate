package conduit;

import com.intuit.karate.Results;
import com.intuit.karate.Runner;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ConduitRunner {
    @Test
    void testSuiteConduit() {
        Results results = Runner.path("classpath:conduit").tags("~@skipme").parallel(2);
        assertEquals(0, results.getFailCount(), results.getErrorMessages());
    }
}
