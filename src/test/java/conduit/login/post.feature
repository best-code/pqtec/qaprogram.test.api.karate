@Login
Feature: Login functionality tests

  Background:
    * url urlLogin
    * configure headers = { "Content-Type": "application/json"}

  @DoLogin @Positive
  Scenario: Executing a login using valid credentials
    * def loginJson = read('classpath:/conduit/login/validLogin.json')
    * set loginJson.email = karate.get("login.email")
    * set loginJson.password = karate.get("login.password")
    Given path ''
    And request loginJson
    When method post
    Then status 201
    * match response.success == true
    * def token = response.data.token

  @NotValidLogin @Negative
  Scenario: Executing a login using not valid credentials
    * def loginJson = read('classpath:/conduit/login/validLogin.json')
    * set loginJson.email = "emailDeTeste@test.com"
    * set loginJson.password = "passwordErrada"
    Given path ''
    And request loginJson
    When method post
    Then status 404