package conduit.login;

import com.intuit.karate.Results;
import com.intuit.karate.Runner;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LoginRunner {
    @Test
    void testSuiteLogin() {
        Results results = Runner.path("classpath:conduit/login").tags("~@skipme").parallel(2);
        assertEquals(0, results.getFailCount(), results.getErrorMessages());
    }
}
