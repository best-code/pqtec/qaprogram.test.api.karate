@Article
Feature: Articles functional tests for POST request

  Background:
    * url urlArticles
    * def doLogin = call read('classpath:conduit/login/post.feature@DoLogin')
    * def authToken = "Bearer " + doLogin.token
    * configure headers = { "Content-Type": "application/json", Authorization: '#(authToken)' }

  @Positive
  Scenario: Create a simple article
    * def articleJson = read('classpath:conduit/articles/article.json')
    * set articleJson.title = 'My article title'
    * set articleJson.description = 'My article description'
    * set articleJson.body = 'My article body'
    * set articleJson.tagList[0] = 'Tag 1'
    * set articleJson.tagList[1] = 'Tag 2'
    * print articleJson

    Given path ''
    And header karate-name = 'Create Article'
    And request articleJson
    When method POST
    Then status 201
    * match response.success == true
    * match response.data.title == articleJson.title
    * match response.data.description == articleJson.description


  @Negative
  Scenario: Create a article without mandatory fields
    * def articleJson = read('classpath:conduit/articles/article.json')
    * remove articleJson.description
    * set articleJson.title = 'My article title 2'
    * set articleJson.body = 'My article body 2'
    * set articleJson.tagList[0] = 'Tag 3'
    * print articleJson

    Given path ''
    And header karate-name = 'Create Article without mandatory field'
    And request articleJson
    When method POST
    Then status 500
    * match response.success == false
    * match response.message == "ER_NO_DEFAULT_FOR_FIELD: Field 'description' doesn't have a default value"