package conduit.articles;

import com.intuit.karate.Results;
import com.intuit.karate.Runner;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ArticleRunner {
    @Test
    void testSuiteArticle() {
        Results results = Runner.path("classpath:conduit/articles").tags("~@skipme").parallel(2);
        assertEquals(0, results.getFailCount(), results.getErrorMessages());
    }
}
