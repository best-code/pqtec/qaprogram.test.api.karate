## Karate Test Framework

***
## Dependencies
- java 1.8
- maven 3.8.1

## Description
This is a project created for automate the process of test using Karate Framework. 


## Usage
There are a lot of possibilities to execute this project, some of configurations are listed bellow:

Run all TCs:
- mvn clean test -DargLine="-Dkarate.env=e2e" -Dtest=ConduitRunner

Run all TCs filtering by specific tag:
- mvn clean test -DargLine="-Dkarate.env=e2e" -Dtest=ConduitRunner  "-Dkarate.options=--tags @Article"

Run all TCs from specific module using dedicated runner and filtering by specific tag:
- mvn clean test -DargLine="-Dkarate.env=e2e" -Dtest=ArticleRunner
- mvn clean test -DargLine="-Dkarate.env=e2e" -Dtest=ArticleRunner "-Dkarate.options=--tags @Positive"